import commonjs from '@rollup/plugin-commonjs';
import run from '@rollup/plugin-run';
import typescript from '@rollup/plugin-typescript';
import json from '@rollup/plugin-json';
import nodePolyfills from 'rollup-plugin-node-polyfills';
import { terser } from 'rollup-plugin-terser';
import babel from '@rollup/plugin-babel';
import pkg from './package.json';

const prod = process.env.NODE_ENV === 'production'
const appArgs = (process.env.APP_ARGS || '').split('#').filter(Boolean)

export default {
    input: 'src/index.ts',
    output: {
        file: 'dist/index.js',
        format: 'cjs',
        sourcemap: !prod
    },
    external: [...Object.keys(pkg.dependencies)],
    plugins: [
        nodePolyfills(),
        commonjs(),
        json(),
        typescript({ module: 'esnext' }),
        babel({ exclude: /node_modules/, babelHelpers: 'bundled' }),
        process.env.ROLLUP_RUN && run({ args: appArgs }),
        prod && terser(),
    ],
}