import { FastifyInstance } from "fastify"

export default (app: FastifyInstance) => {
    app.get('/', async request => {
        return { hello: "world" }
    })

    app.get('/foo', async (request) => {
        await new Promise(resolve => setTimeout(resolve, 1000))

        return 'bar'
    })
}