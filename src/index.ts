import fastify from "fastify"
import routes from "./routes"

const app = fastify({
    logger: {
        prettyPrint: true,
    },
    disableRequestLogging: true
})

routes(app)

app.listen(8000, (err, addr) => {
    if (err) {
        app.log.error(err)
        process.exit(1)
    }
})